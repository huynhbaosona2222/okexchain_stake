<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Okexchain</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body class="top-navigation">

    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom white-bg">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                        <a href="#" class="navbar-brand">OKExchain (by Sonhuynh)</a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a aria-expanded="false" role="button" href="#">FLUX STAKING</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="">
                                <a aria-expanded="false" role="button" href="https://www.okex.com/">OKEx</a>
                            </li>
                        </ul>
                        <!-- <ul class="nav navbar-nav">
                            <li class="">
                                <a aria-expanded="false" role="button" href="layouts.html">Bitcoin Cash</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="">
                                <a aria-expanded="false" role="button" href="layouts.html">Bitcoin Cash SV</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <a href="login.html">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul> -->
                    </div>
                </nav>
            </div>


            <div class=" text-center animated fadeInDown" style="margin-top: 80px;">
                <h3 class="font-bold">OKExchain X Flux</h3>
                <p style="font-size: 50px;" id="demo">Check Staking Value</p>
                <br>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Deposite History</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <!-- <div class="col-sm-5 m-b-xs"><select class="input-sm form-control input-s-sm inline">
                                        <option value="0">Option 1</option>
                                        <option value="1">Option 2</option>
                                        <option value="2">Option 3</option>
                                        <option value="3">Option 4</option>
                                    </select>
                                    </div>
                                    <div class="col-sm-4 m-b-xs">
                                        <div data-toggle="buttons" class="btn-group">
                                            <label class="btn btn-sm btn-white"> <input type="radio" id="option1" name="options"> Day </label>
                                            <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
                                            <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>
                                        </div>
                                    </div> -->
                                    <div class="col-sm-8">
                                        <form action="/" method="get">
                                            <div class="input-group">
                                                <input type="text" placeholder="Metamask OKExchain mainnet address" class="input-sm form-control" name="address"> <span class="input-group-btn">
                                                    <button type="submit" class="btn btn-sm btn-primary"> Go!</button> </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-6">
                                                <img src="image/USDT.png" alt="" srcset="" width="100%">
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Time </th>
                                                        <th>USDT</th>
                                                        <th>FLUX</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: left;">SUM</td>
                                                        <td style="text-align: left;">#</td>
                                                        <td style="text-align: left;">{{$sum_usdt}}</td>
                                                        <td style="text-align: left;">{{$sum_flux_usdt}}</td>
                                                    </tr>
                                                    @foreach($last_result as $key => $item)
                                                    @if(isset($item['USDT']))
                                                    <tr>
                                                        <td style="text-align: left;">{{$key}}</td>
                                                        <td style="text-align: left;">{{$item['time']}}</td>
                                                        <td style="text-align: left;">{{$item['USDT']}}</td>
                                                        <td style="text-align: left;">{{$item['FLUX']}}</td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-6">
                                                <img src="image/USDT.png" alt="" srcset="" width="100%">
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Time </th>
                                                        <th>OKT</th>
                                                        <th>FLUX</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: left;">SUM</td>
                                                        <td style="text-align: left;">#</td>
                                                        <td style="text-align: left;">{{$sum_okt}}</td>
                                                        <td style="text-align: left;">{{$sum_flux_okt}}</td>
                                                    </tr>
                                                    @foreach($last_result as $key => $item)
                                                    @if(isset($item['OKT']))
                                                    <tr>
                                                        <td style="text-align: left;">{{$key}}</td>
                                                        <td style="text-align: left;">{{$item['time']}}</td>
                                                        <td style="text-align: left;">{{$item['OKT']}}</td>
                                                        <td style="text-align: left;">{{$item['FLUX']}}</td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
</body>

</html>
