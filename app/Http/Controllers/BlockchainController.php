<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;

class BlockchainController extends Controller
{
    public function ttt(Request $request)
    {
        if (!$request->address) {
            return view('demo')->with([
                'last_result'=> [],
                'sum_flux_okt' => 0,
                'sum_flux_usdt' => 0,
                'sum_okt' => 0,
                'sum_usdt' => 0
            ]);
        }
        $address = strtolower($request->address);
        $stop = false;
        $filter = [];
        $page = 0;
        while (!$stop) {

            $offset = $page*100;
            $get_list = "https://www.oklink.com/api/explorer/v1/okexchain/addresses/".$address."/transfers/condition?&offset=".$offset."&limit=100&tokenAddress=&to=".$address."&from=&tokenType=OIP20";
            $ch = curl_init();

            // set url
            curl_setopt($ch, CURLOPT_URL, $get_list);

            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // $output contains the output string
            $output = curl_exec($ch);
            $result = json_decode($output);

            // close curl resource to free up system resources
            curl_close($ch);
            $hits = $result->data->hits;
            foreach ($hits as $hit) {
                if ($hit->symbol == "SLP") {
                    $trans_hash = 'https://www.oklink.com/api/explorer/v1/okexchain/transfers?offset=0&limit=1000&tranHash='.$hit->txhash;
                    $filter[] = $trans_hash;
                }
            }
            if (count($hits) != 100) {
                $stop = true;
            }
            $page += 1;
        }
        foreach ($filter as $key => $item) {
            $data[$key] = curl_init($item);
            curl_setopt($data[$key], CURLOPT_RETURNTRANSFER, true);
        }
        // build the multi-curl handle, adding both $ch
        $mh = curl_multi_init();
        foreach ($filter as $key => $item) {
            curl_multi_add_handle($mh, $data[$key]);
        }

        // execute all queries simultaneously, and continue when all are complete
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running);

        //close the handles
        foreach ($filter as $key => $item) {
            curl_multi_remove_handle($mh, $data[$key]);
        }
        curl_multi_close($mh);
        // all of our requests are done, we can now access the results
        $last_result = [];
        $sum_okt = 0;
        $sum_usdt = 0;
        $sum_flux_okt = 0;
        $sum_flux_usdt = 0;
        foreach ($filter as $key => $item) {
            $content = json_decode(curl_multi_getcontent($data[$key]));
            $transss = $content->data->hits;
            $a_trans = [];
            foreach ($transss as $i1) {
                if ($i1->symbol == "FLUXK") {
                    $note = $i1->value;
                }
            }
            foreach ($transss as $i) {
                $mil = $i->blocktime;
                $seconds = $mil/1000;
                $dt = new DateTime("@$seconds");  // convert UNIX timestamp to PHP DateTime
                if ($i->symbol == "FLUXK") {
                    $a_trans['time'] = $dt->format('Y-m-d H:i:s');
                    $a_trans['FLUX'] = $i->value;
                }
                if ($i->symbol == "WOKT") {
                    $a_trans['OKT'] = $i->value;
                    $sum_okt += $i->value;
                    $sum_flux_okt += $note;
                } else if ($i->symbol == "USDT") {
                    $a_trans['USDT'] = $i->value;
                    $sum_usdt += $i->value;
                    $sum_flux_usdt += $note;
                }
            }
            if (count($a_trans) != 0) {
                $last_result[] = $a_trans;
            }
        }
        return view('demo')->with([
            'last_result' => $last_result,
            'sum_flux_okt' => $sum_flux_okt,
            'sum_flux_usdt' => $sum_flux_usdt,
            'sum_okt' => $sum_okt,
            'sum_usdt' => $sum_usdt
        ]);
    }
}
